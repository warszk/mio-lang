use std::io;
use mio_lang::scanner::Scanner;
use mio_lang::parser::Parser;
use mio_lang::parser::ast::Program;

fn main() {
    println!("Welcome to mio REPL");
    loop {
        let mut line = String::new();
        let mut program = Program::default();

        io::stdin().read_line(&mut line).unwrap_or_else(|e| panic!(e));
        
        // let mut scanner = Scanner::scan(&line).peekable();
        let tokens = Scanner::scan(&line);
        {
            {
                let mut parser = Parser::new(&mut program, tokens);
                let _ = parser.parse();
            }
        }
        // let 
        // while let Some(data) = scanner.peek() {
        //     match data {
        //         Ok(value) => println!("{:?}", value),
        //         Err(err) => println!("error: {:?}", err)
        //     }
        //     scanner.next();
        // }


    }
}
