pub mod ast;
mod precedence;

use crate::scanner::error::ScannerError;
use crate::scanner::token::{TokenStruct, Token};
use crate::parser::ast::{Statement, Expression, Program, Operator};
use crate::parser::precedence::{Precedence};

use std::iter::{Peekable, Iterator};

type PrefixFn<'a, I> = fn(parser: &mut Parser<'a, I>)  -> Result<Expression, ParserError>;
type InfixFn<'a, I> = fn(parser:&mut Parser<'a, I>, expr: Expression) -> Result<Expression, ParserError>;

#[derive(Debug)]
pub enum ParserError {
    ScannerError(ScannerError),
    InvalidToken,
    UnexpectedEndOfFile,
    Unexpected
}
pub struct Parser<'a, I>
where I: Iterator<Item = Result<TokenStruct, ScannerError>> {
    program: &'a mut Program,
    tokens: Peekable<I>,
    errors: Vec<ParserError>
}

impl<'a, I> Parser<'a, I> 
where I: Iterator<Item = Result<TokenStruct, ScannerError>> {
    pub fn new(program: &'a mut Program, tokens: I) -> Self {
        Parser {
            program,
            tokens: tokens.peekable(),
            errors: Vec::new()
        }
    }

    fn skip_to_valid(&mut self) -> () {
        loop {
            match self.tokens.peek() {
                Some(Ok(TokenStruct { token: Token::Comment, ..})) => {},
                Some(Err(err)) => { self.errors.push(ParserError::ScannerError(err.clone())) },
                _ => return ()
            };
            self.tokens.next();
        }
    }

    fn advance(&mut self) -> Option<TokenStruct> {
        self.skip_to_valid();
        self.tokens.next().map(|t| t.unwrap())
    }

    fn peek_if(&mut self, cond: &Fn(Token) -> bool) -> bool {
        match self.tokens.peek() {
            Some(result) => {
                match result {
                    Ok(ts) => cond(ts.token.clone()),
                    Err(_) => false
                }
            },
            None => false
        }
    }

    fn check_and_advance_if(&mut self, token: Token) -> bool {
        if self.peek_if(&|c| c == token) {
            self.advance();
            true
        } else {
            false
        }
    }

    fn expect_and_advance(&mut self, token: Token) -> Result<(), ParserError> {
        if self.peek_if(&|c| c == token) {
            self.advance();
            Ok(())
        } else {
            Err(ParserError::Unexpected)
        }
    }

    fn parse_statement(&mut self) -> Result<Statement, ParserError> {
        let token = self.peek();
        
        match token {
            Some(TokenStruct {token: Token::Let, ..}) => {
                self.advance();
                self.parse_let_statement()
            },
            Some(TokenStruct {token: Token::Return, ..}) => {
                self.advance();
                self.parse_return_statement()
            },
            Some(_) => {
                self.parse_expression_statement()
            },
            None => Err(ParserError::InvalidToken)
        }
    }

    fn parse_expression_statement(&mut self) -> Result<Statement, ParserError> {
        let expr = self.parse_expression(Precedence::None)?;
        // parse semicolon - should be checked!!!!
        self.check_and_advance_if(Token::Semicolon);
        Ok(Statement::Expression(expr))
    }

    fn parse_expression(&mut self, precedence: Precedence) -> Result<Expression, ParserError> {
        self.parse_precedence(precedence)
    }

    fn parse_let_statement(&mut self) -> Result<Statement, ParserError> {
        // let x = 10; // literal
        // let x = 10+20;  // expression
        let s = self.advance().unwrap();
        let token = s.token;
        let name = match token {
            Token::Identifier(n) => n,
            _ => return Err(ParserError::InvalidToken)
        };

        let expr = if self.check_and_advance_if(Token::Assign) {
            self.parse_expression(Precedence::None)?
        } else {
            return Err(ParserError::InvalidToken)
        };

        // consume semicolon
        self.check_and_advance_if(Token::Semicolon);


        Ok(Statement::Let(name, expr))
    }

    fn parse_return_statement(&mut self) -> Result<Statement, ParserError> {
        let expr = self.parse_expression(Precedence::None)?;

        self.check_and_advance_if(Token::Semicolon);

        Ok(Statement::Return(expr))
    }

    fn peek(&mut self) -> Option<&TokenStruct> {
        match self.tokens.peek() {
            Some(peeked) => {
                match peeked {
                    Ok(t) => Some(t),
                    Err(e) => {
                        self.errors.push(ParserError::ScannerError(e.clone()));
                        None
                    }
                }
            }, 
            None => None
        }
    }

    fn parse_precedence(&mut self, precedence: Precedence) -> Result<Expression, ParserError> {
        let prefix_function = {
            let (_, prefix_function) = {
                let peeked = self.peek().ok_or(ParserError::UnexpectedEndOfFile)?;
                Self::get_prefix_rule(&peeked)
            };
            prefix_function.ok_or_else( || {
                self.advance();
                ParserError::Unexpected
            })?

        };
        let mut left = prefix_function(self)?;

        loop {
            let infix_function = {
                let (infix_rule_precedence, infix_function) = {
                    let peeked = self.peek();
                    match peeked {
                        Some(p) => Self::get_infix_rule(&p),
                        None => break
                    }
                };
                if precedence <= infix_rule_precedence {
                    Some(infix_function).ok_or_else(|| {
                        self.advance().unwrap();
                        ParserError::Unexpected
                    })?
                } else { None }
            };
            match infix_function {
                Some(x) => {
                    left = x(self, left)?;
                },
                None => {
                    break;
                }
            }
        }
        Ok(left)
    }

    fn get_prefix_rule(ts: &TokenStruct) -> (Precedence, Option<PrefixFn<'a, I>>) {
        match ts.token {
            Token::Bang => (Precedence::None, Some(Parser::unary)),
            Token::Minus => (Precedence::Term, Some(Parser::unary)),
            Token::Number(_) => (Precedence::None, Some(Parser::number)),
            Token::Identifier(_) => (Precedence::None, Some(Parser::identifier)),
            Token::True | Token::False | Token::Nil => (Precedence::None, Some(Parser::literal)),
            Token::LParen => (Precedence::Call, Some(Parser::grouping)),
            Token::If | Token::Else => (Precedence::None, Some(Parser::if_expression)),
            _ => (Precedence::None, None)
        }
    }

    fn get_infix_rule(ts: &TokenStruct) -> (Precedence, Option<InfixFn<'a, I>>) {
        match ts.token {
            Token::Minus => (Precedence::Term, Some(Parser::binary)),
            Token::Plus => (Precedence::Term, Some(Parser::binary)),
            Token::Asterisk => (Precedence::Factor, Some(Parser::binary)),
            Token::Slash => (Precedence::Factor, Some(Parser::binary)),
            Token::LessLess => (Precedence::Factor, Some(Parser::binary)),
            Token::GreaterGreater => (Precedence::Factor, Some(Parser::binary)),
            Token::Ampersand => (Precedence::Factor, Some(Parser::binary)),
            Token::Pipe => (Precedence::Factor, Some(Parser::binary)),
            Token::Equal | Token::NotEqual => (Precedence::Equality, Some(Parser::binary)),
            Token::Greater | Token::GreaterEqual | Token::Less | Token::LessEqual => (Precedence::Comparison, Some(Parser::binary)),
            Token::Or => (Precedence::Or, Some(Parser::binary)),
            Token::And => (Precedence::And, Some(Parser::binary)),
            Token::Elvis => (Precedence::Or, Some(Parser::binary)),      
            _ => (Precedence::None, None)
        }
    }

    fn number(&mut self) -> Result<Expression, ParserError> {
        let data = match self.advance() {
            Some(ts) => match ts {
                TokenStruct { token: Token::Number(num), ..} => {
                    Expression::NumericLiteral(num)
                },
                _ => return Err(ParserError::Unexpected)
            },
            None => return Err(ParserError::Unexpected)
        };
        Ok(data)
    }

    fn literal(&mut self) -> Result<Expression, ParserError> {
        let data = match self.advance() {
            Some(ts) => match ts {
                TokenStruct { token: Token::True, ..} => {
                    Expression::BooleanLiteral(true)
                },
                TokenStruct { token: Token::False, ..} => {
                    Expression::BooleanLiteral(false)
                },
                TokenStruct { token: Token::Nil, ..} => {
                    Expression::Nil
                },
                _ => return Err(ParserError::Unexpected)
            },
            None => return Err(ParserError::Unexpected)
        };
        Ok(data)
    }

    fn grouping(&mut self) -> Result<Expression, ParserError> {
        self.advance();
        let expr = self.parse_expression(Precedence::Assignment)?;
        self.advance(); // we should check if it's really )
        Ok(expr)
    }

    fn identifier(&mut self) -> Result<Expression, ParserError> {
        let ident = self.advance().unwrap();
        match ident {
           TokenStruct { token: Token::Identifier(name), ..} => Ok(Expression::Identifier(name)),
           _ => Err(ParserError::Unexpected) 
        }
    }

    fn if_expression(&mut self) -> Result<Expression, ParserError> {
        // check if LParen
        self.advance();
        self.expect_and_advance(Token::LParen)?;
        let condition = self.parse_expression(Precedence::None)?;
        self.expect_and_advance(Token::RParen)?;
        self.expect_and_advance(Token::LBrace)?;
        let then_statement = self.parse_block_statement()?;
        self.expect_and_advance(Token::RBrace)?;

        let else_statement = if self.check_and_advance_if(Token::Else) {
            self.expect_and_advance(Token::LBrace)?;
            let s = self.parse_block_statement()?;
            self.expect_and_advance(Token::RBrace)?;
            Some(s)
        } else {
            None
        };
        Ok(Expression::If(Box::new(condition), Box::new(then_statement), Box::new(else_statement)))
    }

    fn fun_expression(&mut self) -> Result<Expression, ParserError> {
        
        unimplemented!()
    }

    fn parse_block_statement(&mut self) -> Result<Vec<Statement>, ParserError> {
        let mut stmts = Vec::new();

        loop {
            if self.check_and_advance_if(Token::RBrace) { break }
            let stmt = self.parse_statement()?;
            stmts.push(stmt);
        }

        Ok(stmts)
    }

    fn unary(&mut self) -> Result<Expression, ParserError> {
        let operator = match self.advance() {
            Some(token) => (match token {
                TokenStruct {token: Token::Bang, ..} => Operator::Not,
                TokenStruct {token: Token::Minus, ..} => Operator::Negate,
                _ => unreachable!()
            }),
            _ => unreachable!()
        };

        let right = self.parse_expression(Precedence::Unary)?;
        Ok(Expression::Unary(operator, Box::new(right)))
    }

    fn binary(&mut self, left: Expression) -> Result<Expression, ParserError> {
        let current = self.advance();

        let (operator, precedence) = if let Some(t)  = current {
            let op = match t {
                TokenStruct {token: Token::Plus, ..} => Operator::Add,
                TokenStruct {token: Token::Minus, ..} => Operator::Subtract,
                TokenStruct {token: Token::Asterisk, ..} => Operator::Multiply,
                TokenStruct {token: Token::Slash, ..} => Operator::Divide,
                TokenStruct {token: Token::Equal, ..} => Operator::Equal,
                TokenStruct {token: Token::NotEqual, ..} => Operator::NotEqual,
                TokenStruct {token: Token::Greater, ..} => Operator::Greater,
                TokenStruct {token: Token::GreaterEqual, ..} => Operator::GreaterEqual,
                TokenStruct {token: Token::GreaterGreater, ..} => Operator::LeftShift,
                TokenStruct {token: Token::Less, ..} => Operator::Less,
                TokenStruct {token: Token::LessEqual, ..} => Operator::LessEqual,
                TokenStruct {token: Token::LessLess, ..} => Operator::RightShift,
                TokenStruct {token: Token::Or, ..} => Operator::Or,
                TokenStruct {token: Token::Pipe, ..} => Operator::BitwiseOr,
                TokenStruct {token: Token::And, ..} => Operator::And,
                TokenStruct {token: Token::Ampersand, ..} => Operator::BitwiseAnd,
                TokenStruct {token: Token::Elvis, ..} => Operator::Elvis,
                _ => unreachable!()
            };
            let (precedence, _) = Self::get_infix_rule(&t);
            (op, precedence.next())
        } else {
            unreachable!()
        };

        let right = self.parse_expression(precedence)?;
        Ok(Expression::Binary(operator, Box::new(left), Box::new(right)))
    }

    pub fn parse(&mut self) -> Result<(), ParserError> {
        while let Some(_) = self.tokens.peek() {
            let stmt = self.parse_statement();
            match stmt {
                Ok(s) => self.program.add(s),
                Err(e) => return Err(e)
            }
        }
        println!("-> {:?}", self.program);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::scanner::Scanner;

  #[test]
  fn it_should_parse_correct_line() {
    let line = "let x = 10;return 10;";
    let mut program = Program::default();
    let tokens = Scanner::scan(line);
    {
        {
            let mut parser = Parser::new(&mut program, tokens);
            let _ = parser.parse();
        }
    }

    assert_eq!(program.statements[0], Statement::Let("x".to_string(), Expression::NumericLiteral(10.0)));
    assert_eq!(program.statements[1], Statement::Return(Expression::NumericLiteral(10.0)));

  }

   #[test]
  fn it_should_parse_expression() {
    let line = "!10;";
    let mut program = Program::default();
    let tokens = Scanner::scan(line);
    {
        {
            let mut parser = Parser::new(&mut program, tokens);
            let _ = parser.parse();
        }
    }

    println!("{:?}", program);
  }

  #[test]
  fn it_should_parse_if_expression() {
    let line = "let y = if(x > 20) { 10 } else { 20 };";
    let mut program = Program::default();
    let tokens = Scanner::scan(line);
    {
        {
            let mut parser = Parser::new(&mut program, tokens);
            let _ = parser.parse();
        }
    }

    println!("== {:?}", program);
  }
}