use num_traits::{FromPrimitive, ToPrimitive};
use num_derive::{FromPrimitive, ToPrimitive};

#[derive(PartialEq, PartialOrd, FromPrimitive, ToPrimitive, Clone, Copy, Debug)]
pub enum Precedence {
    None,
    Assignment, // =
    Or, // or 
    And, // and
    Equality, // == !=
    Comparison, // < > <= >=
    Term, // + -
    Factor, // * /  
    Unary, // ! - +
    Call, // . () []
    Primary,
}

impl Precedence {
    /// Returns the next (as in the immediately higher) Precedence.
    /// Note that this is not defined for the item with the highest
    /// precedence. In such case you'll get a panic
    pub fn next(self) -> Precedence {
        // This reduces some boilerplate.
        Precedence::from_u8(self.to_u8().unwrap() + 1).unwrap()
    }
}
