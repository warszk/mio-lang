use crate::scanner::token::{Token};

#[derive(Debug, Default)]
pub struct Program {
    pub statements: Vec<Statement>
}

impl Program {
    pub fn add(&mut self, statement: Statement) {
        self.statements.push(statement);
    }
}


#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
    Let(String, Expression),
    Return(Expression),
    Expression(Expression)
}

#[derive(Debug, Clone, PartialEq)]
pub enum Operator {
    Not,
    Negate,
    Subtract,
    Add,
    Multiply,
    Divide,
    Equal,
    NotEqual,
    Greater,
    GreaterEqual,
    LeftShift,
    Less,
    LessEqual,
    RightShift,
    Or,
    And,
    BitwiseOr,
    BitwiseAnd,
    Elvis
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expression {
    NumericLiteral(f64),
    BooleanLiteral(bool),
    Identifier(String),
    Nil,
    Unary(Operator, Box<Expression>),
    Binary(Operator, Box<Expression>, Box<Expression>),
    If(Box<Expression>, Box<Vec<Statement>>, Box<Option<Vec<Statement>>>), //condition, thenStatement, elseStatement
    FunctionLiteral(Vec<String>, Box<Vec<Statement>>)
}
