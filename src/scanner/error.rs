use crate::scanner::position::Position;

#[derive(Debug, PartialEq, Clone)]
pub enum ScannerError {
  MissingStringTerminator(Position),
  UnexpectedCharacter(char, Position),
  NumberConversion(String, Position)
}
