mod position;
pub mod error;
pub mod token;
mod lexer;

use crate::scanner::lexer::Lexer;
use crate::scanner::token::{TokenStruct};
use crate::scanner::error::{ScannerError};

pub struct Scanner<'a> {
  lexer: Lexer<'a>
}

impl<'a> Scanner<'a> {
  pub fn scan(source: &'a str) -> impl Iterator<Item = Result<TokenStruct, ScannerError>> + 'a {
    Scanner {
      lexer: Lexer::new(source)
    }
  }
}

impl<'a> Iterator for Scanner<'a> {
  type Item = Result<TokenStruct, ScannerError>;
  fn next(&mut self) -> Option<Self::Item> {
    self.lexer.next_token()
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::scanner::token::Token;
  use crate::scanner::position::Position;

  #[test]
  fn it_works() {
    let code = r#"let x = 10;
    let y = x * (20 << 1);
    // comments should work as well!
    x = true;
    "#;

    let token_struct = [
      TokenStruct { token: Token::Let, position: Position { line: 1, column: 1 } },
      TokenStruct { token: Token::Identifier("x".to_string()), position: Position { line: 1, column: 4 } },
      TokenStruct { token: Token::Assign, position: Position { line: 1, column: 6 } },
      TokenStruct { token: Token::Number(10.0), position: Position { line: 1, column: 8 } },
      TokenStruct { token: Token::Semicolon, position: Position { line: 1, column: 11 } },
      TokenStruct { token: Token::Let, position: Position { line: 1, column: 12 } },
      TokenStruct { token: Token::Identifier("y".to_string()), position: Position { line: 2, column: 8 } },
      TokenStruct { token: Token::Assign, position: Position { line: 2, column: 10 } },
      TokenStruct { token: Token::Identifier("x".to_string()), position: Position { line: 2, column: 12 } },
      TokenStruct { token: Token::Asterisk, position: Position { line: 2, column: 14 } },
      TokenStruct { token: Token::LParen, position: Position { line: 2, column: 16 } },
      TokenStruct { token: Token::Number(20.0), position: Position { line: 2, column: 18 } },
      TokenStruct { token: Token::LessLess, position: Position { line: 2, column: 20 } },
      TokenStruct { token: Token::Number(1.0), position: Position { line: 2, column: 23 } },
      TokenStruct { token: Token::RParen, position: Position { line: 2, column: 25 } },
      TokenStruct { token: Token::Semicolon, position: Position { line: 2, column: 26 } },
      TokenStruct { token: Token::Comment, position: Position { line: 2, column: 27 } },
      TokenStruct { token: Token::Identifier("x".to_string()), position: Position { line: 3, column: 37 } },
      TokenStruct { token: Token::Assign, position: Position { line: 4, column: 6 } },
      TokenStruct { token: Token::True, position: Position { line: 4, column: 8 } },
      TokenStruct { token: Token::Semicolon, position: Position { line: 4, column: 13 } }
    ];

    let mut scanner = Scanner::scan(code).peekable();
    let mut i = 0;
    while let Some(t) = scanner.peek() {
      match t {
        Ok(ts) => {
          assert_eq!(ts.token, token_struct[i].token);
        },
        Err(_) => {

        }
      }
      i += 1;
      scanner.next();
    }
    assert!(true)
  }
  
}
