use itertools::{MultiPeek, multipeek};
use crate::scanner::position::Position;
use crate::scanner::token::{TokenStruct, Token};
use crate::scanner::error::ScannerError;
use std::str::Chars;

pub struct Lexer<'a> {
  input: MultiPeek<Chars<'a>>,
  current_position: Position,
  current_lexeme: String
}

impl<'a> Lexer<'a> {
  pub fn new(source: &'a str) -> Lexer<'a> {
    Lexer {
      input: multipeek(source.chars()), 
      current_position: Position::initialize(),
      current_lexeme: String::new()
    }
  }
}

fn is_digit(c: char) -> bool {
  c.is_digit(10)
}

fn is_alphanumeric(c: char) -> bool {
  c.is_alphanumeric()
}

impl<'a> Lexer<'a> {
  pub fn next_token(&mut self) -> Option<Result<TokenStruct, ScannerError>> {
    let initial_position = self.current_position;

    self.eat_whitespaces();
    self.current_lexeme.clear();

    let next_char = match self.advance() {
      Some(c) => c,
      None => return None
    };

    let result = match next_char {
      '(' => Ok(Token::LParen),
      ')' => Ok(Token::RParen),
      '{' => Ok(Token::LBrace),
      '}' => Ok(Token::RBrace),
      '+' => Ok(Token::Plus),
      '-' => Ok(Token::Minus),
      '*' => Ok(Token::Asterisk),
      '?' => {
        if self.advance_match(':') {
          Ok(Token::Elvis)
        } else {
          Ok(Token::QuestionMark)
        }
      }
      '/' => {
        if self.advance_match('/') {
          self.advance_until('\n');
          Ok(Token::Comment)
        } else {
          Ok(Token::Slash)
        }
      },
      '|' => {
        if self.advance_match('|') {
          Ok(Token::Or)
        } else {
          Ok(Token::Pipe)
        }
      },
      '&' => {
        if self.advance_match('&') {
          Ok(Token::And)
        } else {
          Ok(Token::Ampersand)
        }
      },
      '^' => Ok(Token::Caret),
      '%' => Ok(Token::Percent),
      ';' => Ok(Token::Semicolon),
      '=' => {
        if self.advance_match('=') {
          Ok(Token::Equal)
        } else {
          Ok(Token::Assign)
        }
      },
      '!' => {
        if self.advance_match('=') {
          Ok(Token::NotEqual)
        } else {
          Ok(Token::Bang)
        }
      },
      '>' => {
        if self.advance_match('=') {
          Ok(Token::GreaterEqual)
        } else if self.advance_match('>') {
          Ok(Token::GreaterGreater)
        } else {
          Ok(Token::Greater)
        }
      },
      '<' => {
        if self.advance_match('=') {
          Ok(Token::LessEqual)
        } else if self.advance_match('<') {
          Ok(Token::LessLess)
        } else {
          Ok(Token::Less)
        }
      },
      c if is_digit(c) => self.number(c),
      c if c.is_alphabetic() => Ok(self.identifier()),
      '\0' => Ok(Token::EOF),
      c => Err(ScannerError::UnexpectedCharacter(c, self.current_position))
    };

    // result.map: Maps a Result<T, E> to Result<U, E> by applying a function to a contained Ok value, leaving an Err value untouched.
    Some(result.map(|token| TokenStruct { token, position: initial_position }))
  }

  fn peek(&mut self) -> Option<&char> {
    self.input.reset_peek();
    self.input.peek()
  }

  fn peek_if(&mut self, check: &Fn(char) -> bool) -> bool {
    self.input.reset_peek();

    match self.input.peek() {
      Some(&c) => check(c),
      None => false
    }
  }

  fn eat_whitespaces(&mut self) {
    while let Some(&ch) = self.peek() {
      if ch.is_whitespace() {
        self.advance();
      } else {
        break;
      }
    }
  }

  fn advance(&mut self)  -> Option<char> {
    let next = self.input.next();

    if let Some(c) = next {
      self.current_lexeme.push(c);

      if c == '\n' {
        self.current_position.add_line();
      } else {
        self.current_position.add_column();
      }
    };

    next
  }

  fn advance_until(&mut self, expected: char) -> () {
    while self.peek_if(&|c| c != expected) {
      self.advance();
    }
  }

  fn advance_while(&mut self, condition: &Fn(char) -> bool) -> () {
    while self.peek_if(condition) {
      self.advance();
    }
  }

  fn advance_match(&mut self, expected: char) -> bool {
    if self.peek_if(&|c| c == expected) {
      self.advance();
      true
    } else {
      false
    }
  }

  fn identifier(&mut self) -> Token {
    self.advance_while(&is_alphanumeric);
    
    match self.current_lexeme.as_ref() {
      "let" => Token::Let,
      "true" => Token::True,
      "false" => Token::False,
      "return" => Token::Return,
      "nil" => Token::Nil,
      "if" => Token::If,
      "else" => Token::Else,
      "fun" => Token::Fun,
      identifier => Token::Identifier(identifier.into())
    }
  }

  fn number(&mut self, c: char) -> Result<Token, ScannerError> {
    let mut s = String::new();
    let mut t = NumberType::Int;

    if c == '0' {
      t = match self.peek() {
        Some('b') => {
          self.advance();
          NumberType::Binary
        },
        Some('o') => {
          self.advance();
          NumberType::Octal
        },
        Some('x') => {
          self.advance();
          NumberType::Hex
        },
        _ => {
          s.push(c);
          NumberType::Int
        }
      }
    } else {
      s.push(c);
    }

    while let Some(&c) = self.peek() {
      if c.is_digit(10) {
        s.push(self.advance().unwrap());
      } else if c == '.' && t == NumberType::Int {
        t = NumberType::Double;
        s.push(self.advance().unwrap());
      } else {
        break;
      }
    }

    match t {
      NumberType::Binary => {
        match i32::from_str_radix(&s, 2) {
          Ok(num) => Ok(Token::Number(f64::from(num))),
          Err(_) => Err(ScannerError::NumberConversion(s, self.current_position))
        }
      },
      NumberType::Octal => {
        match i32::from_str_radix(&s, 8) {
          Ok(num) => Ok(Token::Number(f64::from(num))),
          Err(_) => Err(ScannerError::NumberConversion(s, self.current_position))
        }
      },
      NumberType::Hex => {
        match i32::from_str_radix(&s, 16) {
          Ok(num) => Ok(Token::Number(f64::from(num))),
          Err(_) => Err(ScannerError::NumberConversion(s, self.current_position))
        }
      },
      NumberType::Int => Ok(Token::Number(s.parse().unwrap())),
      NumberType::Double => Ok(Token::Number(s.parse().unwrap()))
    }
  }
}

#[derive(PartialEq, Debug)]
enum NumberType {
  Binary,
  Octal,
  Hex,
  Int,
  Double
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn it_should_parse_correct_line() {
    let line = "10 + 10.0;";
    let mut lexer = Lexer::new(line);
    let mut tokens = Vec::new();
    while let Some(token) = lexer.next_token() {
      tokens.push(token.unwrap())
    }

    assert_eq!(tokens[0].token, Token::Number(10.0));
    assert_eq!(tokens[1].token, Token::Plus);
    assert_eq!(tokens[2].token, Token::Number(10.0));
  }

  #[test]
  fn it_should_parse_correctly_whitespaces() {
    let line = "10+ 20.0*x;";
    let mut lexer = Lexer::new(line);
    let mut tokens = Vec::new();
    while let Some(token) = lexer.next_token() {
      tokens.push(token.unwrap())
    }

    assert_eq!(tokens[0].token, Token::Number(10.0));
    assert_eq!(tokens[1].token, Token::Plus);
    assert_eq!(tokens[2].token, Token::Number(20.0));
    assert_eq!(tokens[3].token, Token::Asterisk);
    assert_eq!(tokens[4].token, Token::Identifier("x".to_string()));
    assert_eq!(tokens[5].token, Token::Semicolon);
  }

  #[test]
  fn it_should_parse_binary_numbers() {
    let line = "0b101;";
    let mut lexer = Lexer::new(line);

    let token = lexer.next_token().unwrap();
    
    assert_eq!(token.unwrap().token, Token::Number(5.0))
  }

  #[test]
  fn it_should_return_scanner_errors() {
    let line = "0b10201;";
    let mut lexer = Lexer::new(line);

    let token = lexer.next_token().unwrap();

    assert!(token.is_err(), true);
    if let Some(ScannerError::NumberConversion(num, _)) = token.err() {
      assert_eq!(num, "10201");
    } else {
      assert!(false);
    };
  }

  #[test]
  fn it_should_parse_keywords() {
      let line = "let x = 10;";
      let mut lexer = Lexer::new(line);

      let mut token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Let);

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Identifier("x".to_string()));

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Assign);

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Number(10.0));
  }

  #[test]
  fn it_should_parse_comment() {
      let line = "let x = 10; // comment line";
      let mut lexer = Lexer::new(line);

      let mut token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Let);

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Identifier("x".to_string()));

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Assign);

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Number(10.0));

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Semicolon);

      token = lexer.next_token().unwrap();
      assert_eq!(token.unwrap().token, Token::Comment);
  }

  #[test]
  fn it_should_parse_multiline() {
    let line = &r#"10+ 20.0*x;
      let b == 10;
      //this is a comment
      let c = b;
    "#;

    let mut lexer = Lexer::new(line);
    let mut tokens = Vec::new();
    while let Some(token) = lexer.next_token() {
      tokens.push(token.unwrap())
    }

    assert_eq!(tokens[0].token, Token::Number(10.0));
    assert_eq!(tokens[1].token, Token::Plus);
    assert_eq!(tokens[2].token, Token::Number(20.0));
    assert_eq!(tokens[3].token, Token::Asterisk);
    assert_eq!(tokens[4].token, Token::Identifier("x".to_string()));
    assert_eq!(tokens[5].token, Token::Semicolon);
    assert_eq!(tokens[6].token, Token::Let);
    assert_eq!(tokens[7].token, Token::Identifier("b".to_string()));
    assert_eq!(tokens[8].token, Token::Equal);
    assert_eq!(tokens[9].token, Token::Number(10.0));
    assert_eq!(tokens[10].token, Token::Semicolon);
    assert_eq!(tokens[11].token, Token::Comment);
    assert_eq!(tokens[12].token, Token::Let);
    assert_eq!(tokens[13].token, Token::Identifier("c".to_string()));
    assert_eq!(tokens[14].token, Token::Assign);
    assert_eq!(tokens[15].token, Token::Identifier("b".to_string()));
    assert_eq!(tokens[16].token, Token::Semicolon);
  }
}
