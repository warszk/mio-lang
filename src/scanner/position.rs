#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Position {
  pub line: usize,
  pub column: usize
}

impl Position {
  pub fn initialize() -> Position {
    Position { line: 1, column: 1 }
  }

  pub fn add_column(&mut self) {
    self.column += 1;
  }

  pub fn add_line(&mut self) {
    self.line += 1;
    self.column = 1;
  }
}

