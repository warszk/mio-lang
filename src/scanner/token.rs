use crate::scanner::position::Position;

#[derive(Debug, PartialEq, Clone)]
pub enum Token {
  EOF,
  Comment,
  Whitespace,

  // identifiers and literals
  Identifier(String),
  Number(f64),

  // operators
  Assign, // =
  Equal, // ==
  NotEqual, // !=
  Greater, // >
  GreaterEqual, // >=
  Less, // <
  LessEqual, // <=
  Plus, // +
  Minus, // -
  Asterisk, // *
  Slash, // /
  GreaterGreater, // >>,
  LessLess, // <<
  Pipe, // |
  Ampersand, // &
  Caret, // ^
  Percent, // %
  Bang, // !
  And, // &&
  Or, // ||,
  QuestionMark, // ?
  Elvis, // ?:

  // delimiters
  Semicolon,

  LParen, // (
  RParen, // )
  LBrace, // {
  RBrace, // }

  // keywords
  True,
  False,
  Let,
  Return,
  Nil,
  If,
  Else,
  Fun
}

#[derive(Debug)]
pub struct TokenStruct {
  pub token: Token,
  pub position: Position
}
